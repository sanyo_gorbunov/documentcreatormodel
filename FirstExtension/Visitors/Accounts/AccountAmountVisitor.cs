﻿namespace FirstExtension.Visitors.Accounts
{
    using DocumentCreatorModel.Visitors.Accounts;
    using Entities;
    using DocumentCreatorModel.StorageKeys;
    using DocumentCreatorModel.Storages;

    public class AccountAmountVisitor : IAccountVisitor<Account>
    {
        public void Compose(Account account, IReadStorage<AccountStorageKey> storage)
        {
            account.Amount = 10.33m;
        }
    }
}
