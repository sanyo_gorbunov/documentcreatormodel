﻿using Castle.Windsor;

namespace FirstExtension
{
    using Entities;
    using Setup;
    using DocumentCreatorModel.Tasks;
    using DocumentCreatorModel.Writers;

    class Program
    {
        static void Main(string[] args)
        {
            var container = new WindsorContainer();

            container.Install(new Installer());

            var task = container.Resolve<ICreateAccountTask<Account>>();

            var account = task.Create(null);

            var writer = container.Resolve<IAccountWriter<Account>>();

            writer.Write(account);
        }
    }
}
