﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace FirstExtension.Setup
{
    using CommonInstaller = DocumentCreatorModel.Setup.Installer;

    public class Installer : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Install(new CommonInstaller());

            container.Register(Classes.FromThisAssembly()
                .Pick()
                .WithServiceAllInterfaces()
                .LifestyleTransient());
        }
    }
}
