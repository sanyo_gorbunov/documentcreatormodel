﻿using System.Collections.Generic;

namespace FirstExtension.Entities
{
    using DocumentCreatorModel.Entities;

    public class Account: IAccount
    {
        public int Id { get; set; }

        public decimal Amount { get; set; }

        public IEnumerable<Order> Orders { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0} Amount: {1}", Id, Amount);
        }
    }
}
