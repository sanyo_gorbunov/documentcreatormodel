﻿using System.Collections.Generic;

namespace SecondExtension.Entities
{
    using DocumentCreatorModel.Entities;

    public class Account: IAccount
    {
        public int Id { get; set; }

        public string Company { get; set; }

        public IEnumerable<Order> Orders { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0} Company: {1}", Id, Company);
        }
    }
}
