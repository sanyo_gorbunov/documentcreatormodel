﻿namespace SecondExtension.Visitors.Accounts
{
    using DocumentCreatorModel.Visitors.Accounts;
    using DocumentCreatorModel.StorageKeys;
    using DocumentCreatorModel.Storages;
    using Entities;

    public class AccountCompanyVisitor : IAccountVisitor<Account>
    {
        public void Compose(Account account, IReadStorage<AccountStorageKey> storage)
        {
            account.Company = "Test";
        }
    }
}
