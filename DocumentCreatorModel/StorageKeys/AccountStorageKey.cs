﻿namespace DocumentCreatorModel.StorageKeys
{
    public enum AccountStorageKey
    {
        Args,
        AccountId,
        AccountOrderIds,
        OrderId,
        AllOrdersWithInstructionDates
    }
}
