﻿using System.Collections.Generic;

namespace DocumentCreatorModel.Tasks
{
    using Visitors.Accounts;
    using Entities;
    using StorageKeys;
    using Storages;
    using Updaters.Accounts;

    public class CreateAccountTask<TAccount> : ICreateAccountTask<TAccount>
        where TAccount: IAccount, new()
    {
        private readonly IStorageFactory _storageFactory;

        private readonly IEnumerable<IAccountUpdater> _accountUpdaters;

        private readonly IEnumerable<IAccountVisitor<TAccount>> _accountVisitors;

        public CreateAccountTask(IStorageFactory storageFactory,
            IEnumerable<IAccountUpdater> accountUpdaters,
            IEnumerable<IAccountVisitor<TAccount>> accountVisitors)
        {
            _storageFactory = storageFactory;
            _accountUpdaters = accountUpdaters;
            _accountVisitors = accountVisitors;
        }

        public TAccount Create(CreateAccountTaskArgs args)
        {
            var account = new TAccount();

            var storage = CreateStorage(args);

            foreach (var accountUpdater in _accountUpdaters)
            {
                accountUpdater.Update(storage);
            }

            foreach (var accountVisitor in _accountVisitors)
            {
                accountVisitor.Compose(account, storage);
            }

            return account;
        }

        private IReadWriteStorage<AccountStorageKey> CreateStorage(CreateAccountTaskArgs args)
        {
            var storage = _storageFactory.Create<AccountStorageKey>();

            storage.Write(AccountStorageKey.Args, args);

            return storage;
        }
    }
}
