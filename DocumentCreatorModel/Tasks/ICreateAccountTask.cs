﻿namespace DocumentCreatorModel.Tasks
{
    using Entities;

    public interface ICreateAccountTask<TAccount> where TAccount: IAccount, new()
    {
        TAccount Create(CreateAccountTaskArgs args);
    }
}
