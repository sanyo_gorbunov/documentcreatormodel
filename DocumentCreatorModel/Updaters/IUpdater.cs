﻿namespace DocumentCreatorModel.Updaters
{
    using Storages;

    public interface IUpdater<TStorageKey>
    {
        void Update(IReadWriteStorage<TStorageKey> storage);
    }
}
