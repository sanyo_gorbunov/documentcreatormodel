﻿namespace DocumentCreatorModel.Updaters.Orders
{
    using StorageKeys;

    public interface IOrderUpdater: IUpdater<AccountStorageKey>
    {
    }
}
