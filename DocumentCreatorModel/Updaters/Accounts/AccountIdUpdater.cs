﻿namespace DocumentCreatorModel.Updaters.Accounts
{
    using StorageKeys;
    using Storages;

    public class AccountIdUpdater : IAccountUpdater
    {
        public void Update(IReadWriteStorage<AccountStorageKey> storage)
        {
            storage.Write(AccountStorageKey.AccountId, 1);
        }
    }
}
