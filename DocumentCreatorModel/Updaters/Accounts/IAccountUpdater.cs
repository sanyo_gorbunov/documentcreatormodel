﻿namespace DocumentCreatorModel.Updaters.Accounts
{
    using StorageKeys;

    public interface IAccountUpdater: IUpdater<AccountStorageKey>
    {
    }
}
