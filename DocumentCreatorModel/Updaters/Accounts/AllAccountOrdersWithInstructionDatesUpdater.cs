﻿using System;
using System.Collections.Generic;

namespace DocumentCreatorModel.Updaters.Accounts
{
    using StorageKeys;
    using Storages;

    public class AllAccountOrdersWithInstructionDatesUpdater : IAccountUpdater
    {
        public void Update(IReadWriteStorage<AccountStorageKey> storage)
        {
            var dict = new Dictionary<int, DateTime>
            {
                { 1, DateTime.Today },
                { 2, DateTime.Today.AddDays(1) },
                { 3, DateTime.Today.AddDays(2) }
            };

            storage.Write(AccountStorageKey.AllOrdersWithInstructionDates, dict);
        }
    }
}
