﻿using DocumentCreatorModel.Storages;

namespace DocumentCreatorModel.Updaters.Accounts
{
    using StorageKeys;

    public class AccountOrdersUpdater : IAccountUpdater
    {
        public void Update(IReadWriteStorage<AccountStorageKey> storage)
        {
            var orders = new[] { 1, 2 };

            storage.Write(AccountStorageKey.AccountOrderIds, orders);
        }
    }
}
