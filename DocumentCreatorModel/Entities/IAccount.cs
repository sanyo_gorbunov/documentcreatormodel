﻿using System.Collections.Generic;

namespace DocumentCreatorModel.Entities
{
    public interface IAccount
    {
        int Id { get; set; }

        IEnumerable<Order> Orders { get; set; }
    }
}
