﻿using System;

namespace DocumentCreatorModel.Entities
{
    public class Order
    {
        public int Id { get; set; }

        public DateTime InstructionDate { get; set; }

        public decimal Amount { get; set; }
    }
}
