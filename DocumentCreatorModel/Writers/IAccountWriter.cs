﻿namespace DocumentCreatorModel.Writers
{
    using Entities;

    public interface IAccountWriter<TAccount> where TAccount: IAccount
    {
        void Write(TAccount account);
    }
}
