﻿using System;

namespace DocumentCreatorModel.Writers
{
    using Entities;

    public class AccountWriter<TAccount> : IAccountWriter<TAccount>
        where TAccount: IAccount
    {
        public void Write(TAccount account)
        {
            Console.WriteLine("Account: {0}", account.ToString());

            Console.WriteLine();

            foreach (var order in account.Orders)
            {
                Console.WriteLine("Order Id: {0}", order.Id);

                Console.WriteLine("Instruction Date: {0}", order.InstructionDate);

                Console.WriteLine("Amount: {0}", order.Amount);

                Console.WriteLine();
            }
        }
    }
}
