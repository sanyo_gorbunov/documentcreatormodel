﻿using System.Collections.Generic;

namespace DocumentCreatorModel.Storages
{
    public class Storage<TStorageKey> : IReadWriteStorage<TStorageKey>
    {
        private readonly IDictionary<TStorageKey, object> _dictionary;

        public Storage()
        {
            _dictionary = new Dictionary<TStorageKey, object>();
        }

        private Storage(IDictionary<TStorageKey, object> dictionary)
        {
            _dictionary = new Dictionary<TStorageKey, object>(dictionary);
        }

        public T Get<T>(TStorageKey key)
        {
            return (T)_dictionary[key];
        }

        public IDictionary<TKey, TValue> GetDictionary<TKey, TValue>(TStorageKey key)
        {
            return (IDictionary<TKey, TValue>)_dictionary[key];
        }

        public IReadWriteStorage<TStorageKey> GetWritableCopy()
        {
            return new Storage<TStorageKey>(_dictionary);
        }

        public void Write<T>(TStorageKey key, T obj)
        {
            _dictionary.Add(key, obj);
        }
    }
}
