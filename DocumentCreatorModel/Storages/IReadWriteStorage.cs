﻿namespace DocumentCreatorModel.Storages
{
    public interface IReadWriteStorage<TStorageKey> : IReadStorage<TStorageKey>
    {
        void Write<T>(TStorageKey key, T obj);
    }
}
