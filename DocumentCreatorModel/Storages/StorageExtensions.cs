﻿using System.Collections.Generic;

namespace DocumentCreatorModel.Storages
{
    public static class StorageExtensions
    {
        public static IEnumerable<T> GetCollection<TStorageKey, T>(
            this IReadStorage<TStorageKey> storage, TStorageKey key)
        {
            return storage.Get<IEnumerable<T>>(key);
        }

        public static IDictionary<TKey, TValue> GetDictionary<TStorageKey, TKey, TValue>(
            this IReadStorage<TStorageKey> storage, TStorageKey key)
        {
            return storage.Get<IDictionary<TKey, TValue>>(key);
        }
    }
}
