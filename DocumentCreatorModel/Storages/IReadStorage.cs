﻿namespace DocumentCreatorModel.Storages
{
    public interface IReadStorage<TStorageKey>
    {
        T Get<T>(TStorageKey key);

        IReadWriteStorage<TStorageKey> GetWritableCopy();
    }
}
