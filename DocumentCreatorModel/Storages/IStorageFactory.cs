﻿namespace DocumentCreatorModel.Storages
{
    public interface IStorageFactory
    {
        IReadWriteStorage<TStorageKey> Create<TStorageKey>();
    }
}
