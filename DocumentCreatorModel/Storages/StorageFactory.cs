﻿namespace DocumentCreatorModel.Storages
{
    public class StorageFactory : IStorageFactory
    {
        public IReadWriteStorage<TStorageKey> Create<TStorageKey>()
        {
            return new Storage<TStorageKey>();
        }
    }
}
