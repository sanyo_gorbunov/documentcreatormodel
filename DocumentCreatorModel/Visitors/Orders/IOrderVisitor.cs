﻿namespace DocumentCreatorModel.Visitors.Orders
{
    using Entities;
    using StorageKeys;

    public interface IOrderVisitor: IVisitor<Order, AccountStorageKey>
    {
    }
}
