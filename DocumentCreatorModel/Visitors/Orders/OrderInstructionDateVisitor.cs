﻿using System;

namespace DocumentCreatorModel.Visitors.Orders
{
    using Entities;
    using StorageKeys;
    using Storages;

    public class OrderInstructionDateVisitor : IOrderVisitor
    {
        public void Compose(Order order, IReadStorage<AccountStorageKey> storage)
        {
            var orderId = storage.Get<int>(AccountStorageKey.OrderId);

            var ordersWithInstructionDates = storage.GetDictionary<AccountStorageKey, int, DateTime>(AccountStorageKey.AllOrdersWithInstructionDates);

            order.InstructionDate = ordersWithInstructionDates[orderId];
        }
    }
}
