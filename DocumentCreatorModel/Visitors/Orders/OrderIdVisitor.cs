﻿namespace DocumentCreatorModel.Visitors.Orders
{
    using Entities;
    using StorageKeys;
    using Storages;

    public class OrderIdVisitor : IOrderVisitor
    {
        public void Compose(Order order, IReadStorage<AccountStorageKey> storage)
        {
            order.Id = storage.Get<int>(AccountStorageKey.OrderId);
        }
    }
}
