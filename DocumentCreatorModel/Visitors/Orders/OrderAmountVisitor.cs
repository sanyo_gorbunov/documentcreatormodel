﻿namespace DocumentCreatorModel.Visitors.Orders
{
    using Entities;
    using StorageKeys;
    using Storages;

    public class OrderAmountVisitor : IOrderVisitor
    {
        public void Compose(Order order, IReadStorage<AccountStorageKey> storage)
        {
            order.Amount = 10.33m;
        }
    }
}
