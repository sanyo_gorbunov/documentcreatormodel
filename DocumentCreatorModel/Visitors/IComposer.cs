﻿namespace DocumentCreatorModel.Visitors
{
    using Storages;

    public interface IVisitor<TElem, TStorageKey>
    {
        void Compose(TElem elem, IReadStorage<TStorageKey> storage);
    }
}
