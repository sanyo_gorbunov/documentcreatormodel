﻿namespace DocumentCreatorModel.Visitors.Accounts
{
    using Entities;
    using StorageKeys;
    using Storages;

    public class AccountIdVisitor<TAccount> : IAccountVisitor<TAccount>
        where TAccount: IAccount
    {
        public void Compose(TAccount account, IReadStorage<AccountStorageKey> storage)
        {
            account.Id = storage.Get<int>(AccountStorageKey.AccountId);
        }
    }
}
