﻿using System.Collections.Generic;

namespace DocumentCreatorModel.Visitors.Accounts
{
    using Entities;
    using Orders;
    using StorageKeys;
    using Storages;
    using Updaters.Orders;

    public class AccountOrdersVisitor<TAccount> : IAccountVisitor<TAccount>
        where TAccount: IAccount
    {
        private readonly IEnumerable<IOrderUpdater> _orderUpdaters;

        private readonly IEnumerable<IOrderVisitor> _orderVisitors;

        public AccountOrdersVisitor(
            IEnumerable<IOrderUpdater> orderUpdaters,
            IEnumerable<IOrderVisitor> orderVisitors)
        {
            _orderUpdaters = orderUpdaters;
            _orderVisitors = orderVisitors;
        }

        public void Compose(TAccount account, IReadStorage<AccountStorageKey> storage)
        {
            var orders = new List<Order>();

            var orderIds = storage.GetCollection<AccountStorageKey, int>(AccountStorageKey.AccountOrderIds);

            foreach (var orderId in orderIds)
            {
                var order = new Order();

                var orderStorage = CreateStorage(storage, orderId);

                foreach (var orderUpdater in _orderUpdaters)
                {
                    orderUpdater.Update(orderStorage);
                }

                foreach (var orderVisitor in _orderVisitors)
                {
                    orderVisitor.Compose(order, orderStorage);
                }

                orders.Add(order);
            }

            account.Orders = orders;
        }

        private IReadWriteStorage<AccountStorageKey> CreateStorage(IReadStorage<AccountStorageKey> storage, int orderId)
        {
            var orderStorage = storage.GetWritableCopy();

            orderStorage.Write(AccountStorageKey.OrderId, orderId);

            return orderStorage;
        }
    }
}
