﻿namespace DocumentCreatorModel.Visitors.Accounts
{
    using Entities;
    using StorageKeys;

    public interface IAccountVisitor<TAccount>: IVisitor<TAccount, AccountStorageKey>
        where TAccount: IAccount
    {
    }
}
